from django import forms
from .models import MataKuliah,Kegiatan,AnggotaKegiatan

class MataKuliahForm(forms.ModelForm):
    class Meta:
        model = MataKuliah
        fields =['nama_matakuliah','dosen','sks','deskripsi','semester_tahun','ruang_kelas']

class KegiatanForm(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = ['nama_kegiatan']

class AnggotaForm(forms.ModelForm):
    class Meta:
        model = AnggotaKegiatan
        fields = ['anggota_kegiatan']