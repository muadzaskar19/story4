from django.db import models
from django.utils import timezone
from datetime import datetime,date

# Create your models here.

class MataKuliah(models.Model):
    nama_matakuliah = models.CharField(max_length=30)
    dosen = models.CharField(max_length=30)
    sks = models.CharField(max_length=2)
    deskripsi = models.CharField(max_length=30)
    semester_tahun = models.CharField(max_length=30)
    ruang_kelas = models.CharField(max_length=10)

    # def __str__(self):
    #     return self.nama_matakuliah

class Post(models.Model):
    author = models.ForeignKey(MataKuliah, on_delete=models.CASCADE)
    content = models.CharField(max_length=200)
    published_date = models.DateTimeField(default=timezone.now)

class Kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length=30)
    
    
class AnggotaKegiatan(models.Model):
    anggota_kegiatan = models.CharField(max_length=30)
    kegiatan = models.ForeignKey(Kegiatan,on_delete=models.CASCADE)


