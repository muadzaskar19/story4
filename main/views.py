from django.shortcuts import render,redirect
from django.core.mail import send_mail
from .models import MataKuliah,Post,Kegiatan,AnggotaKegiatan
from django.http import HttpResponseRedirect,JsonResponse
from .forms import MataKuliahForm,KegiatanForm,AnggotaForm
from django.contrib import messages
import requests
import json


def home(request):
    return render(request, 'main/portofolio.html')

def profile(request):
    return render(request, 'main/profile.html')

def contactMe(request):
    # if request == "POST":
    #     name = request.Post['name'] 
    #     email = request.Post['email']  
    #     subject = request.Post['subject']
    #     message = request.Post['message']        
    #     send_mail(name+subject,message,email,['muadzaskar19@gmail.com'])
    #     return redirect(request, 'main/ContactMe.html')
    # else:
        return render(request, 'main/ContactMe.html')

def index(request):
    return render(request, 'main/index.html')


# def saveform(request):
#     mata_kuliah = MataKuliah.objects.all()
#     response['mata_kuliah'] = mata_kuliah

#     return render(request,'main/kuliah.html',response)
# def form(request):
#     if request.method == 'POST':
#         form = MataKuliahForm()
#         if form.is_valid():
#             form.save()
#     form = MataKuliahForm()
#     return render(request,'main/kuliah.html',{'form':form})


def matakuliah(request):
    form = MataKuliahForm()
    if request.method == 'POST':
        form = MataKuliahForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/matakuliah')
    data = MataKuliah.objects.all()
    context = {'form':form,'data':data}
    return render(request,'main/kuliah.html',context)

def kegiatan(request):
    kegiatan_form = KegiatanForm()
    if request.method == 'POST':
        kegiatan_form = KegiatanForm(request.POST)
        if kegiatan_form.is_valid():
            kegiatan_form.save()
            return redirect('/kegiatan')
    anggota_form = AnggotaForm()
    if request.method == 'POST':
        anggota_form = AnggotaForm(request.POST)
        # if anggota_form.is_valid():
        #     anggota_form.save()
        #     return redirect('/kegiatan')
    # data_anggota = AnggotaKegiatan.objects.all()
    data_kegiatan = Kegiatan.objects.all()
    context_kegiatan = {'kegiatan_form':kegiatan_form,'data_kegiatan':data_kegiatan,'anggota_form':anggota_form}
    return render(request,'main/kegiatan.html',context_kegiatan)

# def anggota(request):
#     anggota_form = AnggotaForm()
#     if request.method == 'POST':
#         anggota_form = AnggotaForm(request.POST)
#         if anggota_form.is_valid():
#             anggota_form.save()
#             return redirect('/kegiatan')
#     data_anggota = AnggotaKegiatan.objects.all()
#     context = {'anggota_form':anggota_form,'data_anggota':data_anggota}
#     return render(request,'main/kegiatan.html',context)

def hapus_kegiatan(request,item_id):
    item = Kegiatan.objects.get(id=item_id)
    item.delete()   
    return redirect('/kegiatan')

# def hapus_anggota(request,item_id):
#     item = anggota.objects.get(id=item_id)
#     item.delete()   
#     return redirect('/kegiatan')

def hapus(request,item_id):
    item = MataKuliah.objects.get(id=item_id)
    item.delete()   
    return redirect('/matakuliah')

def tambah(request):
    data = MataKuliah.objects.all()
    return render(request,'main/kuliah.html',{'data':data})
    # mata_kuliah = MataKuliah.objects.all()
    # response['mata_kuliah'] = mata_kuliah

    # return render(request,'main/kuliah.html',response)
# def detail(request):

#     return render(request,'main/detail.html',{'data',data})

def buku(request):
    response = {}
    return render(request,'main/buku.html',response)

def data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)
    data = json.loads(ret.content)

    if not request.user.is_authenticated:
        i = 0
        for x in data['items']:
            del data['items'][i]['volumeInfo']['imageLinks']['smallThumbnail']
            i += 1
    return JsonResponse(data,safe=False)