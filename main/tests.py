from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import include, path, reverse
from selenium import webdriver
import json
from .models import MataKuliah,Kegiatan,AnggotaKegiatan


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)
    
    def test_url_kuliah_status_200(self):
        response = self.client.get('/matakuliah/')
        self.assertEqual(response.status_code, 200)
        # # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:profile'))
        self.assertEqual(response.status_code, 200)
    
    def test_url_contactme_status_200(self):
        response = self.client.get('/contactMe/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:contactMe'))
        self.assertEqual(response.status_code, 200)
    
    def test_url_index_status_200(self):
        response = self.client.get('/index/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:index'))
        self.assertEqual(response.status_code, 200)

    def test_url_kegiatan_status_200(self):
        response = self.client.get('/kegiatan/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:kegiatan'))
        self.assertEqual(response.status_code, 200)

    def test_url_profile_status_200(self):
        response = self.client.get('/profile/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:profile'))
        self.assertEqual(response.status_code, 200)
    
    def test_url_buku_status_200(self):
        response = self.client.get('/buku/')
        self.assertEqual(response.status_code,200)
    
    def test_data_buku(self):
        url = reverse('main:data')
        self.assertEqual(url, '/data/')


class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())

class TemplateTestCase(TestCase):
    def test_template_main(self):
        response = self.client.get('/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn('',html_kembalian)
        self.assertTemplateUsed(response,'main/portofolio.html')

    def test_template_profile(self):
        response = Client().get('/profile/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn('',html_kembalian)
        self.assertTemplateUsed(response,'main/profile.html')

    def test_template_contactMe(self):
        response = Client().get('/contactMe/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn('',html_kembalian)
        self.assertTemplateUsed(response,'main/ContactMe.html')

    def test_template_index(self):
        response = Client().get('/index/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn('',html_kembalian)
        self.assertTemplateUsed(response,'main/index.html')
    
    def test_template_matakuliah(self):
        response = Client().get('/matakuliah/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn('',html_kembalian)
        self.assertTemplateUsed(response,'main/kuliah.html')
    
    def test_template_kegiatan(self):
        response = Client().get('/kegiatan/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn('',html_kembalian)
        self.assertTemplateUsed(response,'main/kegiatan.html')

    def test_template_buku(self):
        response = Client().get('/buku/')
        html_kembalian = response.content.decode('utf8')
        self.assertTemplateUsed(response,'main/buku.html')


    

    


