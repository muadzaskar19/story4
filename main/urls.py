from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('profile/',views.profile,name='profile'),
    path('contactMe/',views.contactMe,name='contactMe'),
    path('index/',views.index,name='index'),
    path('matakuliah/',views.matakuliah,name='matakuliah'),
    path('delete/<int:item_id>/',views.hapus,name='delete'),
    path('deletekegiatan/<int:item_id>/',views.hapus_kegiatan,name='deletekegiatan'),
    path('deleteanggota/<int:item_id>/',views.hapus_kegiatan,name='delete_anggota'),
    path('kegiatan/',views.kegiatan,name='kegiatan'),
    path('buku/',views.buku),
    path('data/',views.data,name='data')
]
